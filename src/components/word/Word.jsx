import React, { Fragment, useState } from "react";
import Typography from "@material-ui/core/Typography";
import Popover from "@material-ui/core/Popover";
import FormatItalicIcon from "@material-ui/icons/FormatItalic";
import FormatBoldIcon from "@material-ui/icons/FormatBold";
import FormatUnderlinedIcon from "@material-ui/icons/FormatUnderlined";
import Button from "@material-ui/core/Button";
import Synonyms from "../synonims/Synonyms";

export default ({ word }) => {
  const [newWord, setNewWord] = useState(null);
  const selectedWord = newWord || word

  const [anchorEl, setAnchorEl] = useState(null);

  const [italic, setItalic] = useState(false);
  const [bold, setBold] = useState(false);
  const [underline, setUnderline] = useState(false);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);

  return (
    <Fragment>
      <Typography
        variant="body1"
        component="span"
        gutterRight
        onClick={handleClick}
        style={{
          fontStyle: italic ? "italic" : "normal",
          fontWeight: bold ? "bold" : "normal",
          textDecoration: underline ? "underline" : "none"
        }}
      >
        {selectedWord}
      </Typography>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
      >
        <Button onClick={() => setItalic(!italic)}>
          <FormatItalicIcon />
        </Button>
        <Button onClick={() => setBold(!bold)}>
          <FormatBoldIcon />
        </Button>
        <Button onClick={() => setUnderline(!underline)}>
          <FormatUnderlinedIcon />
        </Button>
        <div style={{ minWidth: 200 }}>
          <Synonyms word={selectedWord} onChangeWord={setNewWord}/>
        </div>
      </Popover>{" "}
    </Fragment>
  );
};
