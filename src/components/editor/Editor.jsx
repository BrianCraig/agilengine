import React from "react";
import useFetch from "react-fetch-hook";
import Word from "../word/Word";

export default () => {
  const {
    isLoading,
    data
  } = useFetch(`https://catfact.ninja/fact`);


  if (isLoading) {
    return "Loading your facts...";
  }

  return (
    <div>
      {data.fact.split(" ").map((word, pos) => (
        <Word key={word + pos} word={word} />
      ))}
    </div>
  );
};
