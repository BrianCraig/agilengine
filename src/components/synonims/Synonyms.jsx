import React, { Fragment, useMemo, useCallback, useState } from "react";
import Typography from "@material-ui/core/Typography";
import useFetch from "react-fetch-hook";

// obtained from https://github.com/fantasyui-com/oneof/blob/master/index.js
const oneOf = (list, rand) => {
  if (list == undefined) return null;
  if (list.length === 0) return null;

  var min = 0;
  var max = list.length - 1;

  var idx = Math.floor(rand * (max - min + 1)) + min;

  return list[idx];
};

const rawrFunction = (serviceData, rand) => [
  ...serviceData.slice(0, 3).map(({ word }) => word),
  oneOf(
    ["purr", "prrrprrrr", "meow", "meooooow", "miau", "grrrr", "rawr"],
    rand
  )
];

export default ({ word, onChangeWord }) => {
  const rand = useCallback(Math.random(), []);
  const { isLoading, data } = useFetch(
    `https://api.datamuse.com/words?rel_syn=${word}`
  );
  return isLoading
    ? "Loading synonyms..."
    : rawrFunction(data, rand).map(word => (
        <Fragment>
          <Typography
            variant="body1"
            component="span"
            style={{
              textDecoration: "underline"
            }}
            onClick={() => onChangeWord(word)}
          >
            {word}
          </Typography>{" "}
        </Fragment>
      ));
};
